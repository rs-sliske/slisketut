package com.custard130.scripts.slisketut;

import java.awt.Point;

import org.powerbot.script.rt4.Component;
import org.powerbot.script.rt4.Widget;

public class ID {

	private static final int	COLOR_VALUE_WHEN_ACTIVE_TAB	= 50; //blue value

	public enum ANIM {
		IDLE(-1);

		private final int	ID;

		ANIM(int ID) {
			this.ID = ID;
		}

		public int id() {
			return ID;
		}
	}

	public enum SETTING {
		PROGRESS(406), RUNNING(173);

		private final int	id;

		SETTING(final int id) {
			this.id = id;
		}

		public int ID() {
			return id;
		}

		public int VALUE() {
			return Util.ctx.varpbits.varpbit(id);
		}
	}

	public enum WIDGIT {
		PROGRESS_BAR(371),
		SMITHING(1),
		EQUIPMENT_TAB(1),
		EQUIPMENT_SCREEN(84),
		CREATION(269),
		SETTINGS(0),
		SURVIVAL_GUIDE_1(131),
		INFO_BAR_1(421),
		INFO_BAR_2(372),
		INFO_BAR_3(131),
		CHAT_BAR_1(241),
		CHAT_BAR_2(242),
		CHAT_BAR_3(243),
		CHAT_BAR_4(244);

		final int	id;

		WIDGIT(int id) {
			this.id = id;
		}

		public Widget GET() {
			return Util.ctx.widgets.widget(id);
		}

		public Component COMPONENT(final int compID) {
			return GET().component(compID);
		}
	}

	public enum OBJECTS {
		START_DOOR("Door", 9398),
		TREE("Tree", 9730),
		FIRE("Fire ", 26185),
		SURVIVAL_GATE("Gate", 9470, "between survival and cooking"),
		COOK_DOOR_1("Door ", 9709, "into cooking area"),
		COOK_DOOR_2("Door ", 9710, "out of cooking area"),
		QUEST_DOOR("Door ", 9716, "into quest area"),
		LADDER_DOWN("Ladder", 9726, "down"),
		TIN("Rocks ", 10080, "tin"),
		COPPER("Rocks ", 10079, "copper"),
		FURNACE("Furnace", 10082),
		ANVIL("Anvil ", 2097),
		COMBAT_GATE("Gate ", 9717, "to combat area"),
		RAT_PEN("Gate ", 9720, "into giant rat pen"),
		LADDER_UP("Ladder", 9727, "up"),
		BANK("Bank booth ", 10083),
		ADVISOR_DOOR("Door", 9721, "advisor"),
		EXIT("Door ", 9722, "exit bank"),
		LARGE_DOOR("Large door ", 11710, "chapel"),
		CHURCH("Door ", 9723, "chapel exit");

		private final String	name;
		private final int		id;
		private final String	desc;

		OBJECTS(final String name, final int ID) {
			this.name = name;
			this.id = ID;
			desc = "";
		}

		OBJECTS(final String name, final int ID, final String desc) {
			this.name = name;
			this.id = ID;
			this.desc = desc;
		}

		public String NAME() {
			return name;
		}

		public int ID() {
			return id;
		}

		public String DESCRIPTION() {
			return desc;
		}
	}

	public enum NPCS {
		GUIDE("Runescape Guide", 1548),
		SURVIVAL("Survival Expert", 1546),
		CHEF("Master Chef", 1545),
		QUEST("Quest Guide", 1552),
		MINING("Mining Instructor", 1551),
		COMBAT("Combat Instructor", 1547),
		RAT("Giant rat", 1553),
		FINANCE("Financial Advisor", 1550),
		MAGIC("Magic Instructor", 1549),
		CHICKEN("Chicken", 1556),
		FISHING_SPOT("Fishing spot", 1557);

		private final String	name;
		private final int		id;
		private final String	desc;

		NPCS(final String name, final int ID) {
			this.name = name;
			this.id = ID;
			desc = "";
		}

		NPCS(final String name, final int ID, final String desc) {
			this.name = name;
			this.id = ID;
			this.desc = desc;
		}

		public String NAME() {
			return name;
		}

		public int ID() {
			return id;
		}

		public String DESCRIPTION() {
			return desc;
		}
	}

	public enum ITEMS {
		TINDERBOX("Tinderbox", 590),
		AXE("Bronze axe ", 1351),
		LOGS("Logs ", 2511),
		NET("Small fishing net ", 303),
		RAW_SHRIMP("Raw shrimps ", 2514),
		BURNT_SHRIMP("Burnt shrimp ", 7954),
		SHRIMP("Shrimps ", 315),
		FLOUR("Pot of flour ", 3516),
		WATER("Bucket of water ", 1929),
		DOUGH("Bread dough ", 2307),
		BREAD("Bread ", 2309),
		BURNT_BREAD("Burnt bread ", 0),
		PICKAXE("Bronze pickaxe ", 1265),
		TIN("Tin ore ", 438),
		COPPER("Copper ore ", 436),
		BRONZE("Bronze bar ", 2349),
		HAMMER("Hammer ", 2347),
		DAGGER("Bronze dagger ", 1205),
		SWORD("Bronze sword ", 1277),
		SHIELD("Wooden shield ", 1171),
		ARROWS("Bronze arrow ", 882),
		SHORTBOW("Shortbow ", 841);

		private final String	name;
		private final int		id;
		private final String	desc;

		ITEMS(final String name, final int ID) {
			this.name = name;
			this.id = ID;
			desc = "";
		}

		ITEMS(final String name, final int ID, final String desc) {
			this.name = name;
			this.id = ID;
			this.desc = desc;
		}

		public String NAME() {
			return name;
		}

		public int ID() {
			return id;
		}

		public String DESCRIPTION() {
			return desc;
		}
	}

	protected enum TAB {
		BACKPACK(4),
		STATS(2),
		SETTINGS(12),
		MAGIC(7),
		FRIEND(9),
		IGNORE(10),
		QUEST(3),
		GEAR(5),
		PRAYER(6),
		COMBAT(1),
		LOGOUT(11),
		EMOTE(13),
		MUSIC(14);

		private final static int	X_DIF			= 33;
		private final static int	X_OFFSET		= 541;
		private final static int	Y_DIF			= 297;
		private final static int	Y_OFFSET		= 187;
		private final static int	COLOR_OFFSET	= 17;

		private final int			row;
		private final int			column;
		private final Point			centrePos;
		private final Point			colorPos;

		TAB(int id) {
			row = id / 7;
			column = id % 7;

			centrePos = setPoint();
			colorPos  = setColorPoint();
		}

		private Point setColorPoint(){
			Point temp = centrePos;
			temp.translate(0, -COLOR_OFFSET);
			return temp;
		}
		
		private Point setPoint() {
			int x = (column * X_DIF) + X_OFFSET;
			int y = (row * Y_DIF) + Y_OFFSET;

			return new Point(x, y);
		}

		public boolean open() {
			if(Util.color.getBlue(colorPos)<COLOR_VALUE_WHEN_ACTIVE_TAB){
				return true;
			}
			return Util.ctx.input.click(centrePos, true);
		}

	}

}
