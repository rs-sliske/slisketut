package com.custard130.scripts.slisketut;


import org.powerbot.script.MessageEvent;
import org.powerbot.script.MessageListener;
import org.powerbot.script.Tile;

public abstract class Area extends Util implements MessageListener {

	protected final Tile	centre;
	
	protected static String current = "";

	protected boolean		done	= false;

	protected Area(final Tile tile) {
		this.centre = tile;
	}

	public boolean check() {
		return !done;
	}

	protected void walkTo() {

	}

	public abstract void run() ;

	@Override
	public void messaged(MessageEvent arg0) {
		
	}
	
	public static String getCurrentState(){
		return current;
	}

}
