package com.custard130.scripts.slisketut;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;

import org.powerbot.script.rt4.ClientContext;

public class ColorChecker {

	private ClientContext ctx;
	private Robot screen;

	public ColorChecker(ClientContext ctx) {
		this.ctx = ctx;
		try {
			screen = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	private Coord windowPos() {
		Point mouseLocInBot = ctx.input.getLocation();
		Point mouseLocOnScreen = MouseInfo.getPointerInfo().getLocation();
		int tempX = mouseLocOnScreen.x - mouseLocInBot.x;
		int tempY = mouseLocOnScreen.y - mouseLocInBot.y;
		return new Coord(tempX, tempY);
	}

	private int getColor0(int x, int y) {
		return screen.getPixelColor(x, y).getRGB();
	}

	public int getColor(int x, int y) {
		int tempX = x + windowPos().getX();
		int tempY = y + windowPos().getY();
		return getColor0(tempX, tempY);
	}
	public int getBlue(int x,int y){
		int color = getColor(x,y);
		
		int blue = color & 0x0000ff;
		return blue;
	}
	public int getRed(int x,int y){
		int color = getColor(x,y);
		
		int red = color & 0xff0000;
		return red;
	}
	
	public int getColor(Point p){
		return getColor(p.x,p.y);
	}
	public int getBlue(Point p){
		return getBlue(p.x,p.y);
	}
	public int getRed(Point p){
		return getRed(p.x,p.y);
	}
	

}

class Coord {
	private int x, y;

	public Coord(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

}