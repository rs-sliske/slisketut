package com.custard130.scripts.slisketut.areas;

import java.awt.Point;

import org.powerbot.script.Tile;

import com.custard130.scripts.slisketut.Area;
import com.custard130.scripts.slisketut.Paths.PATH;

public class Starting extends Area {
	private static final int	ACCEPT_BUTTON_COMPONENT_ID	= 99;
	private static final int	OPTIONS_BUTTON_CENTRE_X		= 675;
	private static final int	OPTIONS_BUTTON_CENTRE_Y		= 480;

	public Starting() {
		super(new Tile(3094, 3107, 0));
	}

	@Override
	public void run() {
		final STATE current = getState();
		Area.current=current.name();
		switch (current) {
			case CUSTOMIZE_PLAYER:
				WIDGIT.CREATION.COMPONENT(ACCEPT_BUTTON_COMPONENT_ID).click();
				counter++;
				break;
			case DOOR:
				interactWithObject(OBJECTS.START_DOOR.ID(), "Open");
				break;
			case OPTIONS:
				openOptions();
				break;
			case TALK:
				talkTo(NPCS.GUIDE.ID());
				break;
			case DONE:
				PATH.TO_SURVIVAL.follow();
				done = true;
				break;
			default:
				break;

		}//Util.wait(100);
	}

	private boolean openOptions() {
		final int x = OPTIONS_BUTTON_CENTRE_X + random(10);
		final int y = OPTIONS_BUTTON_CENTRE_Y + random(10);
		final Point p = new Point(x, y);
		counter++;
		return ctx.input.click(p, true);
//		return ctx.game.tab(Game.Tab.OPTIONS);
	}

	private STATE getState() {
		if (WIDGIT.CREATION.COMPONENT(ACCEPT_BUTTON_COMPONENT_ID).visible()) {
			return STATE.CUSTOMIZE_PLAYER;
		}
		if (WIDGIT.INFO_BAR_1.COMPONENT(7).text().toLowerCase().contains("guide to continue")) {
			return STATE.TALK;
		}
		if (WIDGIT.INFO_BAR_1.COMPONENT(7).text().toLowerCase().contains("continue")) {
			return STATE.TALK;
		}
		if (WIDGIT.INFO_BAR_2.COMPONENT(1).text().toLowerCase().contains("getting started")) {
			return STATE.TALK;
		}
		if (WIDGIT.INFO_BAR_2.COMPONENT(1).text().toLowerCase().contains("player controls")) {
			return STATE.OPTIONS;
		}
		if (WIDGIT.INFO_BAR_1.COMPONENT(1).text().toLowerCase().contains("scenery")) {
			return STATE.DOOR;
		}
		if (WIDGIT.INFO_BAR_1.COMPONENT(1).text().toLowerCase().contains("moving around")) {
			return STATE.DONE;
		}

		return STATE.TALK;
	}

	private enum STATE {
		CUSTOMIZE_PLAYER, TALK, OPTIONS, DOOR, DONE;

	}

}
