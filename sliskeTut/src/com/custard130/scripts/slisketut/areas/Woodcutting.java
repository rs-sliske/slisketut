package com.custard130.scripts.slisketut.areas;

import java.awt.Point;

import org.powerbot.script.Tile;
import org.powerbot.script.rt4.Game;

import com.custard130.scripts.slisketut.Area;
import com.custard130.scripts.slisketut.Paths.PATH;

public class Woodcutting extends Area {
	
	private static final int	OPTIONS_BUTTON_CENTRE_X		= 645;
	private static final int	OPTIONS_BUTTON_CENTRE_Y		= 185;

	private enum STATE {
		WALKING, TALKING, CUTTING, LIGHTING, IDLE, DONE;
	};

	private int		tinderbox	= ITEMS.TINDERBOX.ID();
	private int		logs		= ITEMS.LOGS.ID();
	private int		axe			= ITEMS.AXE.ID();

	private int		treeID		= OBJECTS.TREE.ID();

	private boolean	needsInvo	= true;

	public Woodcutting() {
		super(new Tile(3102, 3097, 0));

	}
	private boolean openInvo() {
		final int x = OPTIONS_BUTTON_CENTRE_X + random(10);
		final int y = OPTIONS_BUTTON_CENTRE_Y + random(10);
		final Point p = new Point(x, y);
		counter++;
		return ctx.input.click(p, true);
//		return ctx.game.tab(Game.Tab.OPTIONS);
	}
	public void run() {
		STATE cur = getState();
		current = cur.name();
		switch (cur) {
			case CUTTING:
				if (cutLogs()) {
				}
				break;
			case LIGHTING:
				if (lightLogs())
					needsInvo = false;
				break;
			case IDLE:
				break;
			case TALKING:
				talk();
				break;
			case WALKING:
				walk();
				break;
			case DONE:
				done = true;
			default:
				break;
		}
	}

	private boolean lightLogs() {
		return combineItems(logs, tinderbox);
	}

	private boolean cutLogs() {
		if (!ctx.game.tab(Game.Tab.INVENTORY)) {
			return false;
		}
		int logCount = count(logs);
		if (WIDGIT.INFO_BAR_2.COMPONENT(1).text().toLowerCase()
				.contains("flashing backpack")) {
			TAB.BACKPACK.open();
			return false;
			
		}
		if (interactWithObject(treeID, "Chop down")) {
			return count(logs) > logCount;
		} else {
			return false;
		}
	}

	private boolean talk() {
		return talkTo(NPCS.SURVIVAL.ID());
	}

	private boolean dialogIsClear() {
		// recieving axe and tinderbox
		if (WIDGIT.INFO_BAR_3.COMPONENT(1).text().toLowerCase()
				.contains("the survival guide gives you")) {
			WIDGIT.INFO_BAR_3.COMPONENT(3).click();
			return false;
		}
		if (WIDGIT.INFO_BAR_2.COMPONENT(1).text().toLowerCase()
				.contains("viewing the items")) {
			openInvo();
			return false;
		}
		if (WIDGIT.INFO_BAR_2.COMPONENT(1).text().toLowerCase()
				.contains("you get some logs")) {
			WIDGIT.INFO_BAR_2.COMPONENT(2).click();
			return false;
		}
		if (WIDGIT.INFO_BAR_2.COMPONENT(1).text().toLowerCase()
				.contains("you gained some experience")) {
			ctx.game.tab(Game.Tab.STATS);
			return false;
		}
		

		return true;
	}

	private boolean walk() {
		PATH.TO_SURVIVAL.follow();
		return player().tile().distanceTo(centre) < 3;
	}

	private STATE getState() {
		if (distanceTo(centre) > 5 || distanceTo(centre) < -1) {
			return STATE.WALKING;
		} // player is close to survival expert
		if (WIDGIT.INFO_BAR_2.COMPONENT(0).text().toLowerCase()
				.contains("cut down a tree")) {
			return STATE.CUTTING;
		}
		if (WIDGIT.INFO_BAR_2.COMPONENT(0).text().toLowerCase()
				.contains("making a fire")) {
			return STATE.LIGHTING;
		}
		if (WIDGIT.INFO_BAR_1.COMPONENT(0).text().toLowerCase()
				.contains("your skill stats")) {
			return STATE.DONE;
		}
		if (dialogIsClear()) {
			if (!playerHas(axe)) {
				return STATE.TALKING;
			} // player has an axe
			if (needsInvo) {
				if (ctx.game.tab(Game.Tab.INVENTORY)) {
					if (playerHas(logs)) {
						if (playerHas(tinderbox))
							return STATE.LIGHTING;
						return STATE.TALKING;
					}// player needs logs
					return STATE.CUTTING;
				}// failed to open inventory
				return STATE.TALKING;
			}// doesnt need invo anymore
			if (ctx.game.tab() == Game.Tab.STATS)
				return STATE.DONE;

			return STATE.TALKING;
		}
		return STATE.IDLE;
	}

}
