package com.custard130.scripts.slisketut;

import org.powerbot.script.Tile;
import org.powerbot.script.rt4.TilePath;

public class Paths {
	private static Tile[]	toSurvival	= new Tile[] { new Tile(3098, 3107, 0),
			new Tile(3103, 3103, 0), new Tile(3103, 3096, 0),
			new Tile(3103, 3096, 0) };
	
	public enum PATH{
		
		TO_SURVIVAL(toSurvival);
		
		private final TilePath path;
		
		PATH(Tile[] tiles){
			path = Util.ctx.movement.newTilePath(tiles);
		}
		public boolean follow(){
			return path.traverse();
		}
	}
}
