package com.custard130.scripts.slisketut;

import java.awt.Color;
import java.awt.Graphics;

import org.powerbot.script.MessageEvent;
import org.powerbot.script.MessageListener;
import org.powerbot.script.PaintListener;
import org.powerbot.script.PollingScript;
import org.powerbot.script.Script;


import com.custard130.scripts.slisketut.areas.*;
import com.custard130.scripts.slisketut.ID.*;

@Script.Manifest(
		name = "Sliskes Tutorial Island Solver",
		description = "completes tutorial island while you fap.")

public class SliskeTut extends PollingScript<ClientContext> implements
		PaintListener, MessageListener {

	private STATE	lastState	= STATE.IDLE;

	public SliskeTut() {

	}

	@Override
	public void messaged(MessageEvent e) {

	}

	@Override
	public void repaint(Graphics g) {
		g.setColor(Color.GREEN);
		g.fillRect(50, 50, 200, 50);
		g.setColor(Color.BLACK);
		g.drawString(lastState.name(), 65, 65);
		g.drawString(Area.getCurrentState(), 65, 80);
		g.drawString(Util.getCounter()+"", 65, 95);
	}

	@Override
	public void poll() {
		if (Util.ctx == null) {
			Util.init(ctx);
		}
		STATE current = getState();
		if (!(current == lastState)) {
			System.out.println(current);
		}
		lastState = current;
		if (current.get() == null)
			return;
		if (current.get().check()) {
			current.get().run();
		}
		//Util.wait(100);
	}

	private STATE getState() {
		final int temp = SETTING.PROGRESS.VALUE();
		switch (temp) {
			case 0:
				return STATE.STARTING;
			case 2:
				if(STATE.WOODCUT.get().done)
					return STATE.FISHING;
				return STATE.WOODCUT;
			case 3:
				return STATE.FISHING;
			case 5:
				return STATE.COOKING;
			case 7:
				return STATE.QUEST;
			case 8:
				return STATE.MINING;
			case 10:
				return STATE.COMBAT;
			case 12:
				return STATE.BANK;
			case 15:
				return STATE.PRAYER;
			case 18:
				return STATE.MAGIC;
			case 20:
				return STATE.FINISHING;

		}

		return STATE.IDLE;
	}

	public enum STATE {
		IDLE,
		STARTING(new Starting()),
		WOODCUT(new Woodcutting()),
		FISHING,
		COOKING,
		QUEST,
		MINING,
		COMBAT,
		BANK,
		MAGIC,
		PRAYER,
		WALKING,
		FINISHING;

		private final Area	area;

		STATE(Area area) {
			this.area = area;
		}

		STATE() {
			area = null;
		}

		Area get() {
			return area;
		}
	};

}
