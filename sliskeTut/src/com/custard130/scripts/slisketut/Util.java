package com.custard130.scripts.slisketut;

import java.util.ArrayList;
import java.util.Random;

import org.powerbot.script.Tile;
import org.powerbot.script.Locatable;

import org.powerbot.script.rt4.Component;
import org.powerbot.script.rt4.GameObject;
import org.powerbot.script.rt4.Interactive;
import org.powerbot.script.rt4.Item;
import org.powerbot.script.rt4.Npc;
import org.powerbot.script.rt4.Player;

public class Util extends ID {

	protected static ClientContext		ctx;
	protected static int				counter	= 0;
	public static ColorChecker			color;

	private static ArrayList<WIDGIT>	chatBars;

	private static Random				random;

	public static void init(final ClientContext ctx) {
		Util.ctx = ctx;
		random = new Random();
		color = new ColorChecker(ctx);

		addChatBars();
	}

	public static void reset() {
		counter = 0;
	}

	public static int getCounter() {
		return counter;
	}

	private static void addChatBars() {
		chatBars = new ArrayList<WIDGIT>();

		chatBars.add(WIDGIT.CHAT_BAR_1);
		chatBars.add(WIDGIT.CHAT_BAR_2);
		chatBars.add(WIDGIT.CHAT_BAR_3);
		chatBars.add(WIDGIT.CHAT_BAR_4);
	}

	protected static Item getItemByID(final int id) {
		return ctx.inventory.select().id(id).first().poll();
	}

	protected static GameObject getObjectByID(final int id, final int distance,
			final boolean checkReachable) {
		for (GameObject go : ctx.objects.select().id(id).within(distance)
				.reverse()) {
			if (reachable(go) || !checkReachable) {
				return go;
			}
		}
		return null;
	}

	protected static GameObject getObjectByID(final int id) {
		return getObjectByID(id, false);
	}

	protected static GameObject getObjectByID(final int id,
			final boolean checkReachable) {
		return getObjectByID(id, 10, checkReachable);
	}

	protected static Npc getNPCbyID(final int id) {
		return ctx.npcs.select().id(id).nearest().peek();
	}

	protected static boolean playerHas(final int id, final int count) {
		return ctx.inventory.select().id(id).count() >= count;
	}

	public static Player player() {
		return ctx.players.local();
	}

	protected static boolean isIdle() {
		final int anim = ctx.players.local().animation();
		return anim == ANIM.IDLE.id();
	}

	protected static int count(final int id) {
		return ctx.inventory.id(id).select().count();
	}

	protected static boolean combineItems(final int item1, final int item2) {
		final int startCount1 = count(item1);
		final int startCount2 = count(item2);
		final int resourceCountStart = startCount1 + startCount2;

		getItemByID(item1).click();
		getItemByID(item2).click();
		counter += 2;

		final int endCount1 = count(item1);
		final int endCount2 = count(item2);
		final int resourceCountEnd = endCount1 + endCount2;
		return resourceCountEnd != resourceCountStart;
	}

	protected static boolean interactWithObject(final int objectID,
			final String action) {
		final GameObject object = getObjectByID(objectID);
		if (!turnTo(objectID, 'o')) {
			return false;
		}
		if (object.click(action)) {
			wait(5);

			counter++;
			// while (!isIdle()) {
			// }
		}
		return true;
	}

	protected static boolean openTab() {
		return false;
	}

	protected static boolean turnTo(final int id, final char type) {
		Interactive target = null;
		switch (type) {
			case 'o':
				target = getObjectByID(id);
				break;
			case 'n':
				target = getNPCbyID(id);
				break;
			default:
				return false;

		}
		if (target == null) {
			return false;
		} else {
			while (!target.inViewport()) {
				ctx.camera.turnTo((Locatable) target);
			}
			return true;
		}
	}

	protected static boolean clickContinue() {
		// if (WIDGIT.CHAT_BAR_1.COMPONENT(3).visible()) {
		// return WIDGIT.CHAT_BAR_1.COMPONENT(3).click();
		// }
		// if (WIDGIT.CHAT_BAR_2.COMPONENT(4).visible()) {
		// return WIDGIT.CHAT_BAR_2.COMPONENT(4).click();
		// }
		// if (WIDGIT.CHAT_BAR_3.COMPONENT(5).visible()) {
		// return WIDGIT.CHAT_BAR_3.COMPONENT(5).click();
		// }
		// if (WIDGIT.CHAT_BAR_4.COMPONENT(6).visible()) {
		// return WIDGIT.CHAT_BAR_4.COMPONENT(6).click();
		// }
		// if (WIDGIT.SURVIVAL_GUIDE_1.COMPONENT(3).visible()) {
		// return WIDGIT.SURVIVAL_GUIDE_1.COMPONENT(3).click();
		// }

		for (WIDGIT w : chatBars) {
			for (Component c : w.GET().components()) {
				if (c.text().toLowerCase().contains("click here to continue")) {
					counter++;
					return c.click();

				}
			}
		}

		return false;
	}

	protected static boolean talkTo(final int npc) {
		final Npc target = getNPCbyID(npc);
		if (!turnTo(npc, 'n')) {
			return false;
		}
		if (target.click("Talk-to")) {
			counter++;
			wait(5);
			while (!isIdle()) {
				wait(5);
			}
			while (clickContinue()) {
				// try {
				// ctx.wait(abs(random(5)));
				// } catch (InterruptedException e) {
				// e.printStackTrace();
				// }
			}
			return true;
		}
		return false;
	}

	protected static boolean walkTo(Tile target) {
		counter++;
		return ctx.movement.findPath(target).traverse();
	}

	protected static int distanceTo(Tile target) {
		return ctx.movement.distance(target);
	}

	protected static int random(final int variation) {
		final double temp = random.nextDouble() * variation * 2;
		return (int) (temp - variation);
	}

	protected static boolean reachable(Locatable target) {
		return ctx.movement.reachable(player(), target);
	}

	public static int abs(int value) {
		while (value < 0) {
			value *= -1;
		}
		return value;
	}

	public static boolean wait(final int delay) {
		// try {
		// ctx.wait(abs(random(delay)));
		// } catch (InterruptedException e) {
		// return false;
		// //e.printStackTrace();
		// }
		return true;
	}

	protected static boolean playerHas(final int itemID) {
		return playerHas(itemID, 1);
	}

	protected static boolean playerHas(final int... ids) {
		for (int id : ids) {
			if (!playerHas(id))
				return false;
		}
		return true;
	}

	protected static int getColor(final int x, final int y) {
		return color.getColor(x, y);
	}
}
